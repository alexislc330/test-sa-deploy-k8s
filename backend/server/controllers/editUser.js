// controllers/editUser.js

const pool = require('../database/conection');

// Función para editar un usuario existente
async function editUserController(req, res) {
    const { id, correo_electronico, edad, fecha_nacimiento, pais, nombre, apellido, foto_perfil, nombre_usuario } = req.body;

    try {
        const query = `
            UPDATE usuario
            SET correo_electronico = $1,
                edad = $2,
                fecha_nacimiento = $3,
                pais = $4,
                nombre = $5,
                apellido = $6,
                foto_perfil = $7,
                nombre_usuario = $8
            WHERE idusuario = $9
            RETURNING *
        `;

        const values = [correo_electronico, edad, fecha_nacimiento, pais, nombre, apellido, Buffer.from(foto_perfil, 'base64'), nombre_usuario, id];

        const result = await pool.query(query, values);

        if (result.rowCount === 0) {
            return res.status(404).json({ error: 'Usuario no encontrado' });
        }

        return res.status(200).json(result.rows[0]);
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        return res.status(500).json({ error: 'Error al editar el usuario' });
    }
}
module.exports = { editUserController };

