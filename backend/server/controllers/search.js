// controllers/search.js

const { searchUsers } = require('../database/models/searchUser');

async function searchController(req, res) {
    const { query } = req.params;

    try {
        const users = await searchUsers(query);
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({
            message: 'Error buscando usuarios',
            error: error.message
        });
    }
}

module.exports = {
    searchController,
};
