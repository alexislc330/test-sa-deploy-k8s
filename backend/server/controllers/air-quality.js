const pool = require('../database/conection');
const fetch = require('node-fetch');
require('dotenv').config();

const token = process.env.TOKEN_API;

const getAllCountries = async (req, res) => {
    try {
        const { rows } = await pool.query('select * from pais p order by nombre asc;');
        
        if (!rows || rows.length === 0) {
            return res.status(404).json({
                success: false,
                message: 'No se encontraron países'
            });
        }
       
        return res.status(200).json({
            success: true,
            data: rows
        });
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        return res.status(500).json({
            success: false,
            message: 'Error ejecutando la consulta',
            error: error.message 
        });
    }
};

const getAirQualityFromCoordinates = async (lat, lon) => {
    try {
        const response = await fetch(`https://api.waqi.info/feed/geo:${lat};${lon}/?token=${token}`);
        const data = await response.json();
        if (data.status === 'ok') {
            return data.data;
        } else {
            throw new Error('Error fetching data');
        }
    } catch (error) {
        throw error;
    }
};

const getAirQuality = async (req, res) => {
    const { lat, lon, usuario } = req.query;
    if (!lat || !lon || !usuario) {
        return res.status(400).send('Por favor proporcione latitud, longitud y usuario');
    }

    try {
        const airQualityData = await getAirQualityFromCoordinates(lat, lon);

        // Inserción en la tabla bitacora
        const insertQuery = `
            INSERT INTO public.bitacora (latitud, longitud, aqui, usuario) 
            VALUES ($1, $2, $3, $4)
            RETURNING id_bitacora
        `;
        const insertValues = [lat, lon, airQualityData.aqi, usuario];
        const insertResult = await pool.query(insertQuery, insertValues);
            
        if (insertResult.rowCount === 0) {
            return res.status(500).json({
                success: false,
                message: 'Error al insertar en la bitácora'
            });
        }

        res.json({
            success: true,
            data: airQualityData,
            bitacoraId: insertResult.rows[0].id_bitacora
        });
    } catch (error) {
        console.error('Error fetching air quality data or inserting into bitacora:', error);
        res.status(500).send('Error al obtener los datos de calidad del aire o al insertar en la bitácora');
    }
};

module.exports = {
    getAllCountries,
    getAirQuality
};
