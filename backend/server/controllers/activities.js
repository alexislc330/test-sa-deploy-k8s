//modelo de actividades
const activities = require('../database/models/activities');
const {getActivities, insertActivity, insertUserActivity, updateAsignacionActividad} = activities;

// Obtener las actividades de un usuario
async function getUserActivities(req, res) {
    try {
        const rows = await getActivities(req.params.userid);
        return res.status(200).json({
            success: true,
            data: rows
        });
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        return res.status(500).json({
            success: false,
            message: 'Error ejecutando la consulta',
            error: error.message
        });
    }
}

async function insertAdmActivity(req, res) {
    try {
        let foto = req.body.foto;
        let descripcion = req.body.descripcion;
        let titulo = req.body.titulo;

        const rows = await insertActivity(titulo, descripcion, foto);
        return res.status(200).json({
            success: true,
            data: rows
        });
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        return res.status(500).json({
            success: false,
            message: 'Error ejecutando la consulta',
            error: error.message
        });
    }
}




async function asingarActividad(req, res) {
    try {
        let userid = req.body.id_user;
        let activityid = req.body.id_actividad;
        let visitado = req.body.visitado;

        if (visitado === undefined || visitado === null) {
            const rows = await insertUserActivity(userid, activityid);
            return res.status(200).json({
                success: true,
                data: rows
            });
        }else {
            const rows = await updateAsignacionActividad(userid, activityid, visitado);
            return res.status(200).json({
                success: true,
                data: rows
            });
        }
    }catch (error) {
        console.error('Error ejecutando la consulta:', error);
        return res.status(500).json({
            success: false,
            message: 'Error ejecutando la consulta',
            error: error.message
        });
    }
}

module.exports = {getUserActivities,
    insertAdmActivity,
    asingarActividad
}