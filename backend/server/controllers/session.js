const user = require("../database/models/user");
const jwt = require("jsonwebtoken");
//importar libreria para encriptar
const bcrypt = require("bcrypt");

const { emailLogin } = require("../utils/mail");
const { findAll, findByUsername, updatePassword } = user;

const secret = process.env.SECRET;
const expiracion = process.env.EXPIRACION || 0.2;

async function login(req, res) {
  const { username, password } = req.body;
  const user = await findByUsername(username);
  const match = await bcrypt.compare(password, user[0].contrasenia);

  if (user.length === 0) {
    res.status(404).json({ message: "Usuario no encontrado", status: false });
  } else {
    if (match) {
      token = tokenGenerator(username, user[0].nombre);
      tokenRecuperacion = tokenGenerator(username, user[0].nombre);
      let ip = req.connection.remoteAddress;
      verificarTokenLocal(tokenRecuperacion);
      // Si la dirección IP es en formato IPv6, conviértela a IPv4
      if (ip.substr(0, 7) === "::ffff:") {
        ip = ip.substr(7);
      }
      if (ip === '::1') {
        ip = '127.0.0.1';
      }
    
      emailLogin({
        email: user[0].correo_electronico,
        nombre: user[0].nombre_usuario,
        ip: ip, // Utiliza la dirección IP en formato IPv4
        dispositivo: req.headers["user-agent"], // Obtén la información del dispositivo del encabezado 'user-agent'
        token: tokenRecuperacion,
      });
      res
        .status(200)
        .json({
          message: "Usuario autenticado",
          token: token,
          status: true,
          userId: user[0].idusuario,
          username: user[0].nombre_usuario,
        });
    } else {
      res.status(401).json({ message: "Contraseña incorrecta", status: false });
    }
  }
}

function tokenGenerator(sub, name) {
  const token = jwt.sign(
    { sub, name, exp: Date.now() + 60 * expiracion * 1000 },
    secret
  );
  return token;
}

function verificarToken(req, res) {
  const token = req.headers.authorization.split(" ")[1];
  try {
    const decoded = jwt.verify(token, secret);
    console.log(decoded);
    if (Date.now() >= decoded.exp) {
      res.status(401).json({ message: "Token expirado" });
    } else {
      res.status(200).json({ message: "Token válido" });
    }
  } catch (error) {
    console.error("Error en la verificación del token:", error);
    res.status(401).json({ message: "Token inválido" });
    throw error;
  }
}

function verificarTokenLocal(token) {
  try {
    const decoded = jwt.verify(token, secret);
    console.log(decoded);
    if (Date.now() >= decoded.exp) {
      return false;
    } else {
      return true;
    }
  } catch (error) {
    console.error("Error en la verificación del token:", error);
    return false;
  }
}

async function recuperarContra(req, res){
  try{
    const { token } = req.params;
    const decoded = jwt.verify(token, secret);
  
    if (Date.now() >= decoded.exp) {
      return res.status(401).json({ message: "Token expirado" });
    }
    
    const password = req.body.password;
    const hashedPassword = await bcrypt.hash(password, 10);
  
    const update = await updatePassword(decoded.sub, hashedPassword);
    return res.status(200).json({
        success: true,
        data: update
    });

  }catch(error){
    console.error('Error en la verificación del token:', error);
    return res.status(401).json({ message: 'Token inválido' });
  }
  
}

module.exports = { login, 
              verificarToken,
              verificarTokenLocal,
              tokenGenerator,
              recuperarContra
              };
