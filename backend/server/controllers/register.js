//controlador de registro
// controllers/register.js

const regis = require('../database/models/regis');

async function registerController(req, res) {
    const userData = req.body;

    try {
        // Verificar si el correo electrónico ya está registrado
        if (await regis.emailExists(userData.email)) {
            return res.status(400).json({ message: 'Email already registered' });
        }

        // Verificar si el nombre de usuario ya existe
        if (await regis.usernameExists(userData.username)) {
            return res.status(400).json({ message: 'Username already exists' });
        }

        // Registrar el nuevo usuario
        const newUser = await regis.registerUser(userData);
        res.status(200).json({
            message: 'Register successfuly',
            userId: newUser.idusuario
        });
    } catch (error) {
        res.status(401).json({
            message: 'Register failed',
            error: error.message
        });
    }
}

module.exports = {
    registerController,
};
