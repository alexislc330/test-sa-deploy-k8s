// routes/searchRoute.js
const express = require('express');
const router = express.Router();
const { searchController } = require('../controllers/search');

// Ruta para buscar usuarios
router.get('/searchUser/:query', searchController);

module.exports = router;
