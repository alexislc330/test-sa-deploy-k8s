// routes/registerRoute.js
const express = require('express');
const router = express.Router();
const { editUserController } = require('../controllers/editUser');

// Ruta para el registro de usuarios
router.post('/edit', editUserController);

module.exports = router;
