// routes/registerRoute.js
const express = require('express');
const router = express.Router();
const { registerController } = require('../controllers/register');

// Ruta para el registro de usuarios
router.post('/register', registerController);

module.exports = router;
