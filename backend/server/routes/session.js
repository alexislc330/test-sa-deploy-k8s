const express = require('express');
const router = express.Router();
const { login, verificarToken, recuperarContra } = require('../controllers/session');
const {verifyToken} = require('../middleware/index');

router.post('/login', login);
router.get('/verificar', verificarToken);
router.post('/reestablecer-password/:token', recuperarContra);
router.get('/profile', verifyToken, (req, res) => {
    res.json({ message: 'This is a profile route', user: req.user });
  });


module.exports = router;

