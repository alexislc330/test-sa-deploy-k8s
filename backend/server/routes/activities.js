const express = require('express');
const router = express.Router();
const { getUserActivities, insertAdmActivity, asingarActividad } = require('../controllers/activities');

// Ruta para obtener las actividades de un usuario por medio de id
router.get('/activities/:userid', getUserActivities);

router.post('/activities', insertAdmActivity);
router.post('/actividades_usuario', asingarActividad);


module.exports = router;
