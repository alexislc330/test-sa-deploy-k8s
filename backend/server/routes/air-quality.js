const express = require('express');
const { getAllCountries, getAirQuality } = require('../controllers/air-quality');

const router = express.Router();

router.get('/countries', getAllCountries);
router.get('/airquality-country', getAirQuality);

module.exports = router;
