// test/airQualityService.test.js
const request = require('supertest');
const sinon = require('sinon');
const app = require('../microservices/air-qualityService');
const pool = require('../database/conection');

let chai;
let expect;

(async () => {
  chai = await import('chai');
  expect = chai.expect;
})();

describe('Air Quality Service', () => {
    let poolQueryStub;

    before(async () => {
        poolQueryStub = sinon.stub(pool, 'query').resolves({ rows: [{ id: 1, nombre: 'Guatemala' }] });
        await (async () => {})();  // Forzamos la ejecución de la importación dinámica
    });

    after(() => {
        poolQueryStub.restore();
    });

    it('should return a list of countries on GET /air-quality/countries', (done) => {
        request(app)
            .get('/air-quality/countries')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect((res) => {
                expect(res.body).to.have.property('success', true);
                expect(res.body.data).to.be.an('array');
                expect(res.body.data[0]).to.have.property('nombre', 'Guatemala');
            })
            .end(done);
    });
});
