const request = require('supertest');
const express = require('express');
const cors = require('cors');
const routerAuth =  require('../routes/session');
const app = express();

app.use(cors());
app.use(express.json());

const port = 4000;

app.use(routerAuth);
app.get('/', (req, res) => {
  res.send('Hello World!');
});

describe('GET /', () => {
  it('responds with Hello World!', (done) => {
    request(app)
      .get('/')
      .expect('Content-Type', /text\/html/)
      .expect(200)
      .expect((res) => {
        if (res.text !== 'Hello World!') throw new Error('Response does not match expected text');
      })
      .end(done);
  });
});
