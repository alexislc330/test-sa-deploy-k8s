// test/activitiesService.test.js
const request = require('supertest');
const sinon = require('sinon');
const app = require('../microservices/activitiesService');
const activities = require('../database/models/activities');

let chai;
let expect;

before(async () => {
  chai = await import('chai');
  expect = chai.expect;
});

describe('Activities Service', () => {
  let getActivitiesStub;

  before(() => {
    getActivitiesStub = sinon.stub(activities, 'getActivities').resolves([
      {
        id_actividades: 1,
        titulo: 'Evento de apertura',
        descripcion: 'este evento es el de apertura para las reuniones principales',
        usuario: 18,
        visitado: true,
        foto: null  // Simulamos un valor nulo para foto
      }
    ]);
  });

  after(() => {
    getActivitiesStub.restore();
  });

  it('should return a list of activities for user 18 on GET /activities/18', (done) => {
    request(app)
      .get('/activities/18')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((res) => {
        expect(res.body).to.have.property('success', true);
        expect(res.body.data).to.be.an('array');
        expect(res.body.data[0]).to.have.property('titulo', 'Evento de apertura');
        expect(res.body.data[0]).to.have.property('descripcion', 'este evento es el de apertura para las reuniones principales');
        expect(res.body.data[0]).to.have.property('usuario', 18);
        expect(res.body.data[0]).to.have.property('visitado', true);
        // Permitimos que la propiedad 'foto' sea 'null' o un valor específico
        expect(res.body.data[0]).to.have.property('foto').that.is.oneOf([null, 'url_de_la_foto']);
      })
      .end(done);
  });
});
 