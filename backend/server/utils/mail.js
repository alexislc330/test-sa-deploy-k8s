const nodemailer = require("nodemailer");

const URL_FRONTEND = process.env.URL_FRONTEND;
const PORT_FRONTEND = process.env.PORT_FRONTEND;

const emailLogin = async (datos) => {
  const transport = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  const { email, nombre, ip, dispositivo, token } = datos;
  console.log(ip);
  // Obtener la fecha y hora actual en la zona horaria de Guatemala
  const now = new Date();
  const fecha = now.toLocaleString("es-GT", {
    timeZone: "America/Guatemala",
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });

  await transport.sendMail({
    from: "no-reply@greenPulse.com",
    to: email,
    subject: "Nuevo inicio de sesión en tu cuenta de greenPulse",
    text: `Hola ${nombre},\n\nSu cuenta greenPulse de ser firmada desde un dispositivo nuevo.\n\nSu cuenta: ${email}\n${fecha}\nDispositivo: ${dispositivo}\nIP: ${ip}\n\nSi no reconoces esta actividad, por favor contacta al soporte inmediatamente.`,
    html: `
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Baloo+Thambi+2&display=swap" rel="stylesheet">
  <title>Nuevo inicio de sesión en tu cuenta de greenPulse</title>
  <style>
    .header-div,
    .card-header {
      background-color: #22BC66;
      border-radius: 15px 15px 0px 0px;
      height: 50%;
      width: 100%;
    }

    .header-tittle,
    .card-header {
      text-align: left;
      padding-left: 1em;
      font-size: 30px;
      color: whitesmoke;
      font-family: 'Baloo Thambi 2';
    }

    .body-div {
      font-size: 18px;
      color: black;
      font-family: 'Baloo Thambi 2';
      color: #fff;
      margin: 0px auto;
      padding: 5px;
    }

    .leftDiv {
      width: 20%;
      float: left;
    }

    .rightDiv {
      width: 80%;
      color: black;
      float: right;
    }

    .img {
      display: block;
      margin: auto;
    }

    .footer-tittle {
      text-align: center;
      padding-left: 1em;
      font-size: 15px;
      color: whitesmoke;
      font-family: 'Baloo Thambi 2';
    }

    .footer-div {
      background-color: #22BC66;
      height: 5%;
      width: 100%;
      display: flex;
      justify-content: center;
    }

    .button {
      background-color: #3869D4;
      border-top: 10px solid #3869D4;
      border-right: 18px solid #3869D4;
      border-bottom: 10px solid #3869D4;
      border-left: 18px solid #3869D4;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
    }

    .button-green {
      background-color: #22BC66;
      text-align: center;
      color: whitesmoke;
      border-top: 10px solid #22BC66;
      border-right: 18px solid #22BC66;
      border-bottom: 10px solid #22BC66;
      border-left: 18px solid #22BC66;
    }

    .container-centered {
      display: flex;
      justify-content: center;
      font-family: 'Baloo Thambi 2';
    }

    pre {
      text-align: left;
      white-space: pre-line;
    }

    .tab {
      display: inline-block;
      margin-left: 40px;
    }

    .badge {
      border-radius: 15px; 
      padding-left: 10px; 
      padding-right: 10px;
      color: #fff;
    }

    .badge-success {
      background-color: green;
    }

    .badge-danger {
      background-color: #d9534f; 
    }
  </style>
</head>

<body style="margin:0;padding:0;">
  <div>
    <div class="header-div">
      <br>
      <span class="header-tittle">Nuevo inicio de sesión en tu cuenta de greenPulse</span>
    </div>
    <div class="body-div">
      <br>
      <br>
      <div class="leftDiv">
        <img class="img" src="https://media.istockphoto.com/id/1045368942/vector/abstract-green-leaf-logo-icon-vector-design-ecology-icon-set-eco-icon.jpg?s=612x612&w=0&k=20&c=XIfHMI8r1G73blCpCBFmLIxCtOLx8qX0O3mZC9csRLs=" alt="meet" width="75%" height="75%" alt="meet">
      </div>
      
      
      <div class="rightDiv">
        <strong>Hola ${nombre},</strong>
        <p>Su cuenta greenPulse acaba de ser firmada desde un dispositivo nuevo.</p>
        <div class="mb-2">
          <strong>Su cuenta:</strong> ${email}
        </div>
        <div class="mb-2">
          <strong>Fecha:</strong> ${fecha}
        </div>
        <div class="mb-2">
          <strong>Dispositivo:</strong> ${dispositivo}
        </div>
        <div class="mb-2">
          <strong>IP:</strong> ${ip}
        </div>
        <p>Si no reconoces esta actividad, por favor cambia la contraseña dando clic <a href="${URL_FRONTEND}:${PORT_FRONTEND}/restart-password/${token}">aquí</a>.</p>
      </div>
    </div>
    <br>
    <div class="body-div">
      <div class="leftDiv">
      </div>
      <div class="rightDiv">
        Favor no responder a este correo.
        <br>
        <p>Cordialmente,</p>
      </div>
    </div>
    <br>
    <br>
    <br>

  </div>
  <div class="footer-div">
    <br>
    <span class="footer-tittle bg-green-400	">&copy;2024 GreenPulse S.A . Todos los derechos reservados.</span>
  </div>
</body>

</html>
      `,
  });
};

module.exports = {
  emailLogin,
};
