//servidor
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 4000;
//conectar a la base de datos en postgresql
const pool = require('./database/conection');
const usuario = require('./database/models/user');

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello World!');
}
);

//rutas
app.use('', require('./routes/session'));

//rutas de actividades
app.use('', require('./routes/activities'));

// Usar la ruta de busqueda de usuarios para las solicitudes a /searchUser
app.use('', require('./routes/searchRoute')); 

// Usar la ruta de registro para las solicitudes a /register
app.use('', require('./routes/registerRoute'));

app.listen(port, () => {
    console.log(`Servidor corriendo en http://localhost:${port}`);
}
);

pool.connect((err) => {
    if (err) {
        console.error('Error de conexion:', err);
    } else {
        console.log('Conexion a la base de datos exitosa');
    }
});


