// models/regis.js

const pool = require('../conection');
const bcrypt = require('bcrypt');

// Función para registrar un nuevo usuario
async function registerUser(userData) {
    const { username, password, name, lastName, profileDescription, base64_profile_pic, email, age, birthDate, country } = userData;
    
    // Encriptar la contraseña
    const hashedPassword = await bcrypt.hash(password, 10);

    try {
        const query = `
            INSERT INTO usuario (nombre, apellido, descripcion_perfil, foto_perfil, correo_electronico, edad, fecha_nacimiento, pais, nombre_usuario, contrasenia, tipo_usuario)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, 2)
            RETURNING idusuario
        `;
        const values = [name, lastName, profileDescription, Buffer.from(base64_profile_pic, 'base64'), email, age, birthDate, country, username, hashedPassword];
        const result = await pool.query(query, values);
        return result.rows[0];
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error;
    }
}

// Función para verificar si un correo electrónico ya está registrado
async function emailExists(email) {
    try {
        const query = 'SELECT idusuario FROM usuario WHERE correo_electronico = $1';
        const result = await pool.query(query, [email]);
        return result.rowCount > 0;
    } catch (error) {
        console.error('Error verificando el correo electrónico:', error);
        throw error;
    }
}

// Función para verificar si un nombre de usuario ya existe
async function usernameExists(username) {
    try {
        const query = `SELECT idusuario FROM usuario WHERE nombre_usuario = $1`;
        const result = await pool.query(query, [username]);
        return result.rowCount > 0;
    } catch (error) {
        console.error('Error verificando el nombre de usuario:', error);
        throw error;
    }
}

module.exports = {
    registerUser,
    emailExists,
    usernameExists,
};
