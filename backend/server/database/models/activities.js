const pool = require('../conection');


async function getActivities(userid) {
    try {
        const { rows } = await pool.query('select a.id_actividades, a.titulo, a.descripcion, ua.usuario, ua.visitado, a.foto ' +
	'FROM actividades a FULL JOIN usuario_actividades ua ON ua.actividades = a.id_actividades AND (ua.usuario = $1 OR ua.usuario IS NULL) ' +
    'where (ua.usuario = $1 OR ua.usuario IS NULL) order by a.id_actividades asc;', [userid]);
        return rows;
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error; // Lanza el error para que el llamador de la función pueda manejarlo
    }

    
}

async function insertActivity(titulo, descripcion, foto) {
    try {
        const { rows } = await pool.query('INSERT INTO actividades(titulo, descripcion, foto) values($1, $2, $3);', [titulo, descripcion, Buffer.from(foto, 'base64')]);
        return rows;
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error; // Lanza el error para que el llamador de la función pueda manejarlo
    }
}

async function insertUserActivity(userid, activityid) {
    try {
        const { rows } = await pool.query('INSERT INTO usuario_actividades(usuario, actividades, visitado) values($1, $2, true);', [userid, activityid]);
        return rows;
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error; // Lanza el error para que el llamador de la función pueda manejarlo
    }
}

async function updateAsignacionActividad(userid, activityid, visitado) {
    try {
        const { rows } = await pool.query('UPDATE usuario_actividades SET visitado = $1 where usuario = $2 and actividades = $3;', [visitado ,userid, activityid]);
        return rows;
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error; // Lanza el error para que el llamador de la función pueda manejarlo
    }
}

module.exports = {getActivities, 
    insertActivity,
    insertUserActivity, 
    updateAsignacionActividad,
};
