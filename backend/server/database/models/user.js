//se realizan las consultas de los usuarios en el conector de conection.js
const pool = require('../conection');

async function findAll() {
    try {
      const  {rows}  = await pool.query('SELECT * FROM usuario');
      console.log(rows);
      return rows;
    } catch (error) {
      console.error('Error ejecutando la consulta:', error);
      throw error; // Lanza el error para que el llamador de la función pueda manejarlo
    }
  }
  

async function findByUsername(nombre_usuario) {
  try {
    const { rows } = await pool.query('select u.idusuario, u.nombre , u.apellido , u.descripcion_perfil , u.foto_perfil , u.correo_electronico, u.edad , u.fecha_nacimiento , p.nombre as pais, u.nombre_usuario, u.contrasenia, u.fecha_registro, u.tipo_usuario, (SELECT COUNT(ua.actividades) FROM usuario_actividades ua  WHERE ua.usuario = u.idusuario and ua.visitado) AS actividades_count  from usuario u inner join pais p ON p.id_pais = u.pais  WHERE nombre_usuario = $1', [nombre_usuario]);
    return rows;
  } catch (error) {
    console.error('Error ejecutando la consulta:', error);
    throw error; // Lanza el error para que el llamador de la función pueda manejarlo
  }
}

async function updatePassword(nombre_usuario, contrasenia) {
  try {
    const { rows } = await pool.query('UPDATE usuario SET contrasenia = $1 WHERE nombre_usuario = $2', [contrasenia, nombre_usuario]);
    return rows;
  } catch (error) {
    console.error('Error ejecutando la consulta:', error);
    throw error; // Lanza el error para que el llamador de la función pueda manejarlo
  }

}



module.exports = {findAll, 
                    findByUsername, updatePassword
};
