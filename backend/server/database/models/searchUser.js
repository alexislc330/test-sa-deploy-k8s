// models/searchUser.js

const pool = require('../conection');

// Función para buscar usuarios
async function searchUsers(query) {
    const searchQuery = `%${query}%`;

    try {
        const result = await pool.query(`
            SELECT
                u.idusuario,
                u.nombre,
                u.apellido,
                u.descripcion_perfil,
                u.foto_perfil,
                u.correo_electronico,
                u.edad,
                u.fecha_nacimiento,
                p.nombre AS pais,
                u.nombre_usuario,
                (SELECT COUNT(ua.actividades)
                 FROM usuario_actividades ua
                 WHERE ua.usuario = u.idusuario and ua.visitado) AS actividades_count
            FROM usuario u
            JOIN pais p ON u.pais = p.id_pais
            WHERE u.nombre ILIKE $1
               OR u.apellido ILIKE $1
               OR u.nombre_usuario ILIKE $1
        `, [searchQuery]);

        return result.rows;
    } catch (error) {
        console.error('Error ejecutando la consulta:', error);
        throw error;
    }
}

module.exports = {
    searchUsers,
    // otras funciones exportadas
};
