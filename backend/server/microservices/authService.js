// Importar el módulo express
const express = require('express');
const cors = require('cors');
const routerAuth =  require('../routes/session');
const app = express();

app.use(cors());
app.use(express.json());

const port = 4000;



app.use(routerAuth);
app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});
