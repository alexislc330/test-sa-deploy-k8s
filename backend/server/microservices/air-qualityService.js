const express = require('express');
require('dotenv').config();
const cors = require('cors');
const app = express();
const routerAirQuality = require('../routes/air-quality'); 
app.use(cors());

app.use(express.json());

const port = 4002;

app.use('/air-quality', routerAirQuality);

app.listen(port, () => {
    console.log(`Servidor escuchando en http://localhost:${port}`);
});


module.exports = app;