const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 4001;
const { Pool } = require('pg');
require('dotenv').config();


const usuario = require('../database/models/user'); 
const registerRoute = require('../routes/registerRoute'); 
const searchUser = require('../routes/searchRoute');
const editUser = require('../routes/editUserRoute');

const { findAll, findByUsername } = usuario;
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

app.use(cors());
app.use(bodyParser.json());

// Aumentar el límite de tamaño del cuerpo de la solicitud


app.get('/', (req, res) => {
    res.send('Servicio de Gestión de Usuarios');
});

app.use(registerRoute);

app.use(searchUser);
app.use(editUser);



app.get('/usuarios', async (req, res) => {
    const usuarios = await findAll();
    res.json(usuarios);
});

app.get('/usuarios/:nombre_usuario', async (req, res) => {
    const { nombre_usuario } = req.params;
    const usuario = await findByUsername(nombre_usuario);
    res.json(usuario);
});



app.listen(port, () => {
    console.log(`Servicio de Gestión de Usuarios corriendo en http://localhost:${port}`);
});


module.exports = app;