const express = require('express');
require('dotenv').config();
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');

const routerAirQuality = require('../routes/activities'); 
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

app.use(cors());
app.use(bodyParser.json());

const port = 4003;

app.use(routerAirQuality);

app.listen(port, () => {
    console.log(`Servidor actividades escuchando en http://localhost:${port}`);
});

module.exports = app;
