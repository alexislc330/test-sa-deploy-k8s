const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;
const { findByUsername } = require('../database/models/user');


// Middleware para verificar el token JWT
const verifyToken =  async(req, res, next) => {
  const authHeader = req.headers['authorization'];
  
  if (!authHeader) {
    return res.status(403).json({ message: 'No token provided' });
  }

  const token = authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).json({ message: 'No token provided' });
  }

  jwt.verify(token, secret, async (err, decoded) => {
    if (err) {
      return res.status(500).json({ message: 'Failed to authenticate token' });
    }
    
    //req.user = decoded;
    console.log(decoded);
    const username = decoded.sub;
    const user = await findByUsername(username);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    console.log(user);
    req.user = user;

    next();
  });
};




module.exports = {
  verifyToken
};
